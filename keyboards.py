from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardMarkup, KeyboardButton


class Keyboard:
    @classmethod
    def kb_start(cls):
        kb = ReplyKeyboardMarkup(
            resize_keyboard=True,
            one_time_keyboard=True
        )
        buttons = ["Links", "ENV"]
        kb.add(*buttons)
        return kb
    
    @classmethod
    def ikb_start(cls):
        ikb = InlineKeyboardMarkup(row_width=2)
        ib1 = InlineKeyboardButton(
            text='FastAPI_main',
            url='http://10.7.32.32:5055/',
        )
        ib2 = InlineKeyboardButton(
            text='Django',
            url='http://10.7.32.32/',
        )
        ib3 = InlineKeyboardButton(
            text='Flask',
            url='http://192.168.31.104/'
        )
        ib4 = InlineKeyboardButton(
            text='Forum',
            url='http://192.168.31.101/'
        )
        
        ikb.add(ib1, ib2)
        ikb.add(ib3, ib4)
        # ikb.add(ib1).add(ib2)
        return ikb


