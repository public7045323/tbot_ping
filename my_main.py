#!/srv/scripts/Telebot/TBot_env_1506_v2/env/bin/python3.10

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.dispatcher.filters import Text
from aiogram.utils import executor
import asyncio

from config import settings
from keyboards import Keyboard
from snmp import SNMP
from utilits import message


class TestBot:
    def __init__(self, token):
        self.token = token
        self.bot = Bot(token=token)
        self.dp = Dispatcher(self.bot)
        
        self.HELP_COMMAND = "/help - список команд.NNN\
        /start - начать работу с ботом.NNN".split()
        self.HELP_COMMAND = ' '.join(self.HELP_COMMAND).replace('NNN', '\n')

    def execute(self):
        async def start() -> None:
            while True:
                msg_data = ''
                for host, ip in settings.hosts.items():
                    model = await SNMP.model_get(host=host, ip=ip)
                    if model:
                        # print(host, ip, model)
                        temerature = await SNMP.temperature_get(host=host, ip=ip, sw = 1)
                        msg_data += temerature
                        
                # Send message msg_data.
                if len(msg_data) > 4096:
                    msg_to = await message(msg_data)
                else:
                    if msg_data:
                        await self.bot.send_message(settings.chat_id, msg_data)
                
                await asyncio.sleep(900)

        async def on_startup(self) -> None:
            # print('Бот запущен.')
            asyncio.create_task(start())

        executor.start_polling(self.dp, on_startup=on_startup,  skip_updates=True)
        
    
    def main(self):
        # Help def.
        @self.dp.message_handler(commands=['help'])
        async def help(message: types.Message):
            await message.reply(text=f'<em>{self.HELP_COMMAND}</em>', parse_mode='HTML')
        
        # Start def.    
        @self.dp.message_handler(commands=['start'])
        async def cmd_start(message: types.Message):
            if message.from_user.id in settings.white_users_all.values():
                photo = open('./images/bender.jpg', 'rb')
                await self.bot.send_photo(chat_id=message.chat.id, photo=photo)
                # await message.answer(text=f'<em>Привет, добро пожаловать в DL_bot!</em>', parse_mode='HTML')
                # await message.delete()
                # Create kb.
                kb_start = Keyboard.kb_start()
                await self.bot.send_message(chat_id=message.chat.id, 
                                            text=f'<em>Привет, добро пожаловать в WhiteMac!</em>', parse_mode='HTML',
                                            reply_markup=kb_start)

            else:
                await message.answer('Вы не авторизированы.')
            
            # Ext start def.    
            @self.dp.message_handler(Text(equals="Links"))
            async def cmd_links(message: types.Message):
                if message.from_user.id in settings.white_users_all.values():
                    # Add keyboard.
                    ikb_start = Keyboard.ikb_start()
                    await self.bot.send_message(chat_id=message.chat.id, 
                                                text=f'<em>Links</em>', parse_mode='HTML',
                                                reply_markup=ikb_start
                    )
                    
                else:
                    await message.answer('Вы не авторизированы.')
                
            # Ext start def.
            @self.dp.message_handler(Text(equals="ENV"))
            async def cmd_ping(message: types.Message):
                msg_data, msg_no_ping = '', ''
                for host, ip in settings.hosts.items():
                    model = await SNMP.model_get(host=host, ip=ip)
                    if model:
                        # print(host, ip, model)
                        temerature = await SNMP.temperature_get(host=host, ip=ip, sw = 2)
                        msg_data += temerature
                    else:
                        msg_no_ping += f'Недоступен: {host} - {ip}\n'
                
                # Send message msg_data.
                if len(msg_data) > 4096:
                    msg_to = await message(msg_data)
                else:
                    await message.reply(msg_data)
                
                # Send message msg_no_ping
                if msg_no_ping:
                    if len(msg_no_ping) > 4096:
                        msg_to = await message(msg_no_ping)
                    else:
                        await message.reply(msg_no_ping)








if __name__ == '__main__':
    start = TestBot(settings.tbot_key)
    start.main()
    start.execute()