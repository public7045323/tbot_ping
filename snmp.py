from easysnmp import snmp_get, snmp_walk
import re



class SNMP:
    @classmethod
    async def model_get(cls, host: str, ip: str) -> str:
        try:
            model = snmp_get('.1.3.6.1.2.1.1.1.0', hostname=ip, community='public', version=2).value
            return model
        except:
            return None

    
    @classmethod
    async def temperature_get(cls, host: str, ip: str, sw: int):
        msg_txt = ''
        try:
            name_temp_walk = snmp_walk(f'.1.3.6.1.4.1.9.9.13.1.3.1.2', hostname=ip, community='public', version=2)
            for item in name_temp_walk:
                fd_name_temp = re.search('module\s+\d+\s+inlet|air\s+inlet', item.value)
                if fd_name_temp:
                    oid_temp = item.oid.split('.')[-1]
                    temp = snmp_get(f'.1.3.6.1.4.1.9.9.13.1.3.1.3.{oid_temp}', hostname=ip, community='public', version=2)
                    
                    if sw == 1:
                        if int(temp.value) >= 45:
                            msg_txt += '*** ALARM *** ' + host + ':' + ' ' + item.value + ' ' + '-' + ' ' + temp.value + ' ' + '\n'
                            
                    if sw == 2:
                        if int(temp.value) >= 45:
                            msg_txt += '*** ALARM *** ' + host + ':' + ' ' + item.value + ' ' + '-' + ' ' + temp.value + ' ' + '\n'
                        else:
                            msg_txt += host + ':' + ' ' + item.value + ' ' + '-' + ' ' + temp.value + ' ' + '\n'

        except:
            pass
        
        return msg_txt